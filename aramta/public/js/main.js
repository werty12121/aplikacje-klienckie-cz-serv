

var fired = []
var players = []
var gravity_w = 0.02
var client = io();
var camera
var trzesieniekamery = false, tmp_i, cofaniedziala = false, posprzedcof, tmp_c;

$(document).ready(function () {
    client.emit("onconnect", {
        clientName: client.id
    })



    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(
        45, // kąt patrzenia kamery (FOV - field of view)
        16 / 9, // proporcje widoku, powinny odpowiadać proporjom naszego ekranu przeglądarki
        0.1, // minimalna renderowana odległość
        10000 // maxymalna renderowana odległość
    );
    var renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(0xffffff);
    renderer.setSize($(document).width(), $(document).height());
    $("#root").append(renderer.domElement);
    camera.position.set(-100, 100, 100)
    camera.lookAt(scene.position);
    var axes = new THREE.AxesHelper(1000)
    scene.add(axes)

    function render() {
        for (var i = 0; i < fired.length; i++) {
            try {
                fired[i].fired();
                fired[i].gravity()
                
            } catch (e) { }
        }
        if (trzesieniekamery) {
            var x = Math.floor(Math.random() * tmp_i * 80 - 20 * tmp_i)
            var y = Math.floor(Math.random() * tmp_i * 80 - 20 * tmp_i)
            camera.position.set(-100 + x, 100 + y, 100)
            tmp_i = tmp_i / 1.2
        }
        for (var i = 0; i < players.length; i++) {
            if (players[i].cofaniedziala) {
                var vec = new THREE.Vector3(players[i].getobj().rotation.x, players[i].getobj().rotation.y, players[i].getobj().rotation.z)

                var d = 2
                if (tmp_c < 2) {


                    var vecp = new THREE.Vector3(1, 0, 0);
                    vecp.applyAxisAngle(new THREE.Vector3(-1, 0, 0), vec.x);
                    vecp.applyAxisAngle(new THREE.Vector3(0, 1, 0), vec.y);
                    vecp.applyAxisAngle(new THREE.Vector3(0, 0, 1), vec.z);
                    vecp.normalize()

                    players[i].obj.position.set(players[i].getobj().position.x + vecp.x * d, players[i].getobj().position.y + vecp.y * d, players[i].getobj().position.z + vecp.z * d)
                    console.log(players[i].getkola()[0])
                    players[i].getkola()[0].rotation.y += Math.PI / 180 * 5
                    players[i].getkola()[1].rotation.y += Math.PI / 180 * 5
                    tmp_c += 0.1
                } else if (tmp_c < 4) {

                    var vecp = new THREE.Vector3(1, 0, 0);
                    //vecp.applyAxisAngle(new THREE.Vector3(-1, 0, 0), vec.x);
                    vecp.applyAxisAngle(new THREE.Vector3(0, 1, 0), vec.y);
                    //vecp.applyAxisAngle(new THREE.Vector3(0, 0, 1), vec.z);
                    vecp.normalize()
                    players[i].obj.position.set(players[i].getobj().position.x - vecp.x * d, players[i].getobj().position.y - vecp.y * d, players[i].getobj().position.z - vecp.z * d)
                    players[i].getkola()[0].rotation.y -= Math.PI / 180 * 5
                    players[i].getkola()[1].rotation.y -= Math.PI / 180 * 5
                    tmp_c += 0.1
                } else {
                    players[i].cofaniedziala = false
                }
            }
        }
        if (armata1 != undefined) {
            if (armata1.cofaniedziala) {
                var vec = new THREE.Vector3(armata1.getobj().rotation.x, armata1.getobj().rotation.y, armata1.getobj().rotation.z)

                var d = 2
                if (tmp_c < 2) {


                    var vecp = new THREE.Vector3(1, 0, 0);
                    vecp.applyAxisAngle(new THREE.Vector3(-1, 0, 0), vec.x);
                    vecp.applyAxisAngle(new THREE.Vector3(0, 1, 0), vec.y);
                    vecp.applyAxisAngle(new THREE.Vector3(0, 0, 1), vec.z);
                    vecp.normalize()

                    armata1.obj.position.set(armata1.getobj().position.x + vecp.x * d, armata1.getobj().position.y + vecp.y * d, armata1.getobj().position.z + vecp.z * d)
                    console.log(armata1.getkola()[0])
                    armata1.getkola()[0].rotation.y += Math.PI / 180 * 5
                    armata1.getkola()[1].rotation.y += Math.PI / 180 * 5
                    tmp_c += 0.1
                } else if (tmp_c < 4) {

                    var vecp = new THREE.Vector3(1, 0, 0);
                    //vecp.applyAxisAngle(new THREE.Vector3(-1, 0, 0), vec.x);
                    vecp.applyAxisAngle(new THREE.Vector3(0, 1, 0), vec.y);
                    //vecp.applyAxisAngle(new THREE.Vector3(0, 0, 1), vec.z);
                    vecp.normalize()
                    armata1.obj.position.set(armata1.getobj().position.x - vecp.x * d, armata1.getobj().position.y - vecp.y * d, armata1.getobj().position.z - vecp.z * d)
                    armata1.getkola()[0].rotation.y -= Math.PI / 180 * 5
                    armata1.getkola()[1].rotation.y -= Math.PI / 180 * 5
                    tmp_c += 0.1
                } else {
                    armata1.cofaniedziala = false
                }
            }
        }






        requestAnimationFrame(render);

        //ciągłe renderowanie / wyświetlanie widoku sceny nasza kamerą

        renderer.render(scene, camera);
    }

    render();

    var armata1, ui
    client.on('init', (e) => {
        console.log('init')
        armata1 = new aramta({ x: e.pos[0], y: e.pos[1], z: e.pos[2] });
        ui = new UI(armata1);
        for (var i = 0; i < e.others_state.length; i++) {
            players.push(new aramta({ x: e.others_state[i].pos[0], y: e.others_state[i].pos[1], z: e.others_state[i].pos[2] }, e.others_state[i].id))
        }

    })
    client.on('newplayer', (e) => {
        console.log('newplayer', e)
        players.push(new aramta({ x: e.state.pos[0], y: e.state.pos[1], z: e.state.pos[2] }, e.id))

    })
    client.on('removeplayer', (e) => {
        console.log('removeplayer', e)
        console.log(players.length)
        for (var i = 0; i < players.length; i++) {
            console.log(players[i].gid, e.id, players[i].gid == e.id)
            if (players[i].gid == e.id) {
                console.log('remove')
                console.log(scene.children.length)
                scene.remove(players[i].getobj())
                players.splice(i, 1)
                console.log(scene.children.length)
            }
        }
        console.log(players.length)
    })
    client.on('protluf', (e) => {
        for (var i = 0; i < players.length; i++) {
            if (players[i].gid == e.id) {
                players[i].rotatelufy(e.r)
            }
        }
    })
    client.on('protd', (e) => {
        for (var i = 0; i < players.length; i++) {
            if (players[i].gid == e.id) {
                players[i].rotatey(e.r)
            }
        }
    })
    client.on('pfire', (e) => {
        for (var i = 0; i < players.length; i++) {
            if (players[i].gid == e.id) {
                players[i].fire()
            }
        }
    })
    client.on('preload', (e) => {
        for (var i = 0; i < players.length; i++) {
            if (players[i].gid == e.id) {
                players[i].reload()
            }
        }
    })
})