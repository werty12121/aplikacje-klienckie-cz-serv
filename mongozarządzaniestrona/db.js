var MongoClient = require('mongodb').MongoClient;
var ObjectID=require('mongodb').ObjectID
//const url = "mongodb://localhost:27017/";
//const db_name = 'test61117';

var connect = async (url,dbn) => {
    
    let db = await MongoClient.connect(url + dbn);
    return db;
}
var disconnect = (datebase) => {
    try {
        datebase.close()
    } catch (e) {
        console.log(e)
    }
}
module.exports.listDBs = async (url) => {
    
    let database = await connect(url,'');
    try {
        let w=await database.admin().listDatabases()
        return w.databases
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.listCollections=async(url,db)=>{
    let database = await connect(url,db);
    try {
        return await database.listCollections().toArray()
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
   
}
module.exports.dropDataBase=async(url,db)=>{
    let database = await connect(url,db);
    try {
        return await database.dropDatabase()
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
   
}
module.exports.dropCollection=async(url,db,collection_name)=>{
    let database = await connect(url,db);
    try {
        return await database.collection(collection_name).drop()
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.createCollection=async(url,db,collection_name)=>{
    let database = await connect(url,db);
    try {
        return await database.createCollection(collection_name)+'';
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.createDB=async (url,db_name)=>{
    let database = await connect(url,db_name);
    try {
        return await database.createCollection('defaultCollection')+'';
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.get = async (url,collection, requirements, od, ile, sortby, db) => {
    if(requirements._id!=undefined){
        requirements._id=new ObjectID(requirements._id)
    }
    let database = await connect(url,db);
    try {
        let collection_t = await database.collection(collection);
        let wynik;
        if (sortby == undefined) {
            sortby = { _id: 1 }
        }
        if (ile == undefined) {
            ile = 0;
        }
        if (od == undefined) {
            od = 0;
        }
        wynik = await collection_t.find(requirements).sort(sortby).skip(od).limit(ile).toArray();
        return wynik;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.insert = async (url,collection, data, db) => {
    let database = await connect(url,db);
    try {
        let collection_t = await database.collection(collection);
        let wynik = await collection_t.insertOne(data);
        return wynik.result;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.insertmany = async (url,collection, data_array, db) => {
    let database = await connect(url,db);
    try {
        let collection_t = await database.collection(collection);
        let wynik = await collection_t.insertMany(data_array)
        return wynik.result;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.update = async (url,collection, requirements, new_data, db) => {
    
    if(requirements._id!=undefined){
        requirements._id=new ObjectID(requirements._id)
    }
    let database = await connect(url,db);
    try {
        let collection_t = await database.collection(collection);
        let wynik = await collection_t.updateMany(requirements, { $set: new_data });
        return wynik.result;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.delete = async (url,collection, requirements, db) => {
    if(requirements._id!=undefined){
        requirements._id=new ObjectID(requirements._id)
    }
    let database = await connect(url,db);
    try {
        let collection_t = await database.collection(collection);
        let wynik = await collection_t.deleteMany(requirements);
        return wynik.result;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.distinct = async (url,collection, key, requirements, db) => {
    if(requirements._id!=undefined){
        requirements._id=new ObjectID(requirements._id)
    }
    let database = await connect(url,db);
    try {
        let collection_t = await database.collection(collection);
        let wynik = await collection_t.distinct(key, requirements);
        return wynik;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
