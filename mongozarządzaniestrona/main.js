var http = require("http");
var fs = require('fs')
const db = require('./db');

var server = http.createServer(async function (req, res) {
    if (req.method == "GET") {
        fs.exists("public" + req.url, (e) => {
            if (e) {
                try {
                    fs.readFile("public" + req.url, function (error, data) {
                        try {
                            if (req.url.split('.')[1] == 'css') {
                                res.writeHead(200, { 'Content-Type': 'text/css' });
                            } else {
                                res.writeHead(200, { 'Content-Type': 'text/html' });
                            }
                            res.write(data);
                            res.end();
                        } catch (e) {
                            res.end();
                        }
                    })
                } catch (e) {
                    fs.readFile("public/404.html", function (error, data) {
                        res.writeHead(404, { 'Content-Type': 'text/html' });
                        res.write(data);
                        res.end();
                    })
                }

            } else {
                fs.readFile("public/404.html", function (error, data) {
                    res.writeHead(404, { 'Content-Type': 'text/html' });
                    res.write(data);
                    res.end();
                })
            }

        })
    }


})



server.listen(3000, function () {
    console.log("serwer startuje na porcie 3000")
});
var socketio = require("socket.io")
var io = socketio.listen(server)
var clients = []
io.sockets.on("connection", function (client) {
    console.log(client.id)

    client.on('removeDocument', (data) => {
        db.delete(data.url,data.collection,data.req,data.db).then((dane)=>{
            client.emit('removeDocument_response',dane)
        })
    })
    client.on('updateDocument', (data) => {
        db.update(data.url,data.collection,data.req,data.newdata,data.db).then((dane)=>{
            client.emit('updateDocument_response',dane)
        })
    })
    client.on('insertDocument', (data) => {
        db.insertmany(data.url,data.collection,data.document,data.db).then((dane)=>{
            client.emit('insertDocument_response',dane)
        })
    })
    client.on('getDocuments', (data) => {
        db.get(data.url,data.collection,data.req,0,0,undefined,data.db).then((dane)=>{
            client.emit('getDocuments_response',dane)
        })
    })
    client.on('getCollectionsList', (data) => {
        db.listCollections(data.url,data.db).then((dane)=>{
            client.emit('getCollectionsList_response',dane)
        })
    })
    client.on('createCollection', (data) => {
        db.createCollection(data.url,data.db,data.collection).then((dane)=>{
            client.emit('createCollection_response',dane)
        })
    })
    client.on('removeCollection', (data) => {
        db.dropCollection(data.url,data.db,data.collection).then((dane)=>{
            client.emit('removeCollection_response',dane)
        })
    })
    client.on('getDBsList', (data) => {
        db.listDBs(data.url).then((dane)=>{
            client.emit('getDBsList_response',dane)
        })
    })
    client.on('createDB', (data) => {
        db.createDB(data.url,data.db).then((dane)=>{
            client.emit('createDB_response',dane)
        })
    })
    client.on('removeDB', (data) => {
        db.dropDataBase(data.url,data.db).then((dane)=>{
            client.emit('removeDB_response',dane)
        })
    })



})