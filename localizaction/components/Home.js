import React, { Component } from 'react';
import { Button } from 'react-native';
import MyButton from './Button'

export default class Home extends Component {
    constructor(props) {
        super(props)
        this.start = this.start.bind(this);
    }
    start() {
        this.props.navigation.navigate("Main")
    }
    render() {
        return (
            <MyButton name="Start" onclick={this.start} />
        )

    }
}