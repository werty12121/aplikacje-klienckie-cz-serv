import React, { Component } from 'react';
import { Button, FlatList, Text, View, Image, ActivityIndicator, TouchableOpacity } from 'react-native';
import MyButton from './Button'
import { Font } from 'expo';
import { Location, Permissions } from 'expo';
import { AsyncStorage } from "react-native"
import LocationList from './LocationList';

export default class Main extends Component {
    constructor(props) {
        super(props)
        this.pobierzLokalizacje = this.pobierzLokalizacje.bind(this);
        this.usunWszystko = this.usunWszystko.bind(this);
        this.przejdzDoMapy = this.przejdzDoMapy.bind(this);

        this.state = {
            lokalizacje: [],
            refresh: true,
            load: false
        }
    }
    componentDidMount = async () => {
        var lok = await JSON.parse(await AsyncStorage.getItem('lokalizacje'))
        if (lok == null || lok == undefined) {
            lok = []
        }
        this.setState({
            lokalizacje: lok
        })
        await Font.loadAsync({
            'myfont': require('../assets/fonts/Roboto-Light.ttf'),
        });
        this.setState({ fontloaded: true })
    }
    pobierzLokalizacje() {
        this.setState({ load: true })
        this.setPermissions().then(async () => {
            let pos = await Location.getCurrentPositionAsync({})
            var lok = this.state.lokalizacje
            lok.push({ timestamp: pos.timestamp, long: pos.coords.longitude, lat: pos.coords.latitude })
            this.setState({ lokalizacje: lok })
            this.setState({
                refresh: true
            })
            await AsyncStorage.setItem('lokalizacje', JSON.stringify(this.state.lokalizacje))
            this.setState({ load: false })
            alert(JSON.stringify(pos, null, 4))
        })
    }
    usunWszystko = async () => {
        await AsyncStorage.setItem('lokalizacje', "")
        this.setState({
            lokalizacje: [],
            refresh: true
        })
    }
    przejdzDoMapy(zaznaczone) {
        this.props.navigation.navigate("Maps", { zazn: zaznaczone })
    }
    setPermissions = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            alert('odmawiam przydzielenia uprawnień do czytania lokalizacji')
        }
    }


    render() {
        return (
            !this.state.load
                ?
                <View>
                    <View>
                        <MyButton style={{ fontFamily: 'myfont', fontSize: 100 ,fontSize: 40}} name="Pobierz i zapisz lokalizacje" onclick={this.pobierzLokalizacje} />
                        <MyButton style={{ fontFamily: 'myfont', fontSize: 100 ,fontSize: 40}} name="Usun wszsytko" onclick={this.usunWszystko} />
                    </View>
                    <LocationList extraData={this.state} data={this.state.lokalizacje} renderItem={this.renderItemList} przejdzDoMapy={this.przejdzDoMapy} />
                </View>
                :
                <View>
                    <ActivityIndicator size="large" color="#0000ff" />
                </View>


        )
    }
}
