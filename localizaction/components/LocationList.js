import React, { Component } from 'react';
import { Button, FlatList, Text, View, Image, ActivityIndicator, TouchableOpacity } from 'react-native';
import MyButton from './Button'


export default class LocationList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            zaznaczone: []
        }
        this.zaznacz = this.zaznacz.bind(this);
        this.przejdzDoMapy = this.przejdzDoMapy.bind(this);
    }
    renderItemList = ({ item }) => (
        <ListItem item={item} zazn={this.zaznacz}></ListItem>
    )
    zaznacz(data, odz) {

        if (odz) {

            var tz = this.state.zaznaczone
            var index = tz.indexOf(data);
            if (index > -1) {
                tz.splice(index, 1);
            }
        } else {
            var tz = this.state.zaznaczone
            tz.push(data)
            this.setState({ zaznaczone: tz })
        }
        console.log(tz)
    }
    _keyExtractor = (item, index) => item.id;
    przejdzDoMapy() {
        this.props.przejdzDoMapy(this.state.zaznaczone)
    }
    render() {
        return (
            <View>
                <MyButton style={{ fontFamily: 'myfont', fontSize: 100 }} name="Przejdz do mapy" onclick={this.przejdzDoMapy} />
                <FlatList extraData={this.props.extraData} data={this.props.data} renderItem={this.renderItemList} keyExtractor={this._keyExtractor} />
            </View>

        )
    }
}
class ListItem extends Component {
    constructor(props) {
        super(props)
        this.zaznacz = this.zaznacz.bind(this);
    }
    zaznacz(data, odz){
        this.props.zazn(data, odz)
    }
    render() {
        return (
            <View>
                <Image source={require('../assets/images/robot-dev.png')} />
                <Text>{"time: " + this.props.item.timestamp}</Text>
                <Text>{"long: " + this.props.item.long}</Text>
                <Text>{"lat: " + this.props.item.lat}</Text>
                <IamgeSwich data={this.props.item} zaznacz={this.zaznacz} />
            </View>
        )
    }
}
class IamgeSwich extends Component {
    constructor(props) {
        super(props)
        this.state = {
            zazn: false
        }
        this.zaznacz = this.zaznacz.bind(this);
    }
    zaznacz() {
        this.setState({ zazn: !this.state.zazn })
        this.props.zaznacz(this.props.data, this.state.zazn)
    }
    render() {
        return (
            this.state.zazn ?
                <TouchableOpacity onPress={this.zaznacz}>
                    <Image source={require('../assets/images/robot-dev.png')} />
                </TouchableOpacity>
                :
                <TouchableOpacity onPress={this.zaznacz}>
                    <Image source={require('../assets/images/robot-prod.png')} />
                </TouchableOpacity>
        )
    }
}