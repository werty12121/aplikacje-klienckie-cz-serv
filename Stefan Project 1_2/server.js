var http = require("http")
var fs = require("fs");
const PORT = 3000;
var tab = [];
var qs = require("querystring")

var server = http.createServer(function (req, res) {

    if (req.method == "GET") {
        fs.exists("public" + req.url, (e) => {
            if (e) {
                try {
                    fs.readFile("public" + req.url, function (error, data) {
                        try {
                            if (req.url.split('.')[1] == 'css') {
                                res.writeHead(200, { 'Content-Type': 'text/css' });
                            } else if (req.url.split('.')[1] == 'png') {
                                res.writeHead(200, { 'Content-Type': 'image/jpg' });
                            } else {
                                res.writeHead(200, { 'Content-Type': 'text/html' });
                            }
                            res.write(data);
                            res.end();
                        } catch (e) {
                            res.end();
                        }
                    })
                } catch (e) {
                    fs.readFile("public/404.html", function (error, data) {
                        res.writeHead(404, { 'Content-Type': 'text/html' });
                        res.write(data);
                        res.end();
                    })
                }

            } else {
                fs.readFile("public/404.html", function (error, data) {
                    res.writeHead(404, { 'Content-Type': 'text/html' });
                    res.write(data);
                    res.end();
                })
            }

        })
    }
})

server.listen(3000, function () {
    console.log("serwer startuje na porcie 3000")
});
var socketio = require("socket.io")
var io = socketio.listen(server)
var db = require('./db')

var tab_plansza = [1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8];
function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }
    return array;
}


io.sockets.on("connection", function (client) {
    console.log(client.id)
    client.on('newgame', (data) => {
        db.get('Game', {},'Gra').then((dane) => {
            console.log(dane)
            if (dane[0].ruch != 0) {

                tab_plansza = shuffle(tab_plansza);
                tab_plansza = shuffle(tab_plansza);
                var tab2_plansza = []
                for (var i = 0; i < 4; i++) {
                    var posY = 700;
                    tab2_plansza.push([])
                    for (var j = 0; j < 4; j++) {
                        tab2_plansza[i][j] = tab_plansza[i * 4 + j]
                    }
                }
                db.update('Game', {}, { ruch: 0, plansza: tab2_plansza, zaznaczone: [], p1: 0, p2: 0 }, 'Gra')
                client.join('game');
                client.emit('newgame_response', { ruch: 0, plansza: tab2_plansza, zaznaczone: [], p1: 0, p2: 0 })
            } else {
                db.get('Game', {}, 'Gra').then((dane) => {
                    client.join('game');
                    client.emit('join_response', dane[0])
                })
            }

        })

    })
    client.on('move', (data) => {
        console.log('move',data)
        db.get('Game', {}, 'Gra').then((dane) => {
            console.log('1',dane[0].ruch%2 ,data.player)
            if (dane[0].ruch % 2 == data.player) {
                console.log('2')
                if (dane[0].zaznaczone.length == 0 || dane[0].zaznaczone.length == 2) {
                    console.log('3')
                    dane[0].zaznaczone = [data.pos]
                    db.update('Game', {}, dane[0], 'Gra').then(() => {
                        io.to('game').emit('move_response', dane[0])
                    })
                } else {
                    console.log('4')
                    dane[0].ruch++
                    if (dane[0].zaznaczone[0] != data.pos) {

                        dane[0].zaznaczone.push(data.pos)
                        if (dane[0].plansza[dane[0].zaznaczone[0].y][dane[0].zaznaczone[0].x] == dane[0].plansza[dane[0].zaznaczone[1].y][dane[0].zaznaczone[1].x]) {
                            console.log('5')
                            if (dane[0].ruch % 2 == 0) {
                                dane[0].p1 += 1;
                                dane[0].ruch--
                            } else {
                                dane[0].p2 += 1;
                                dane[0].ruch--
                            }
                            dane[0].plansza[dane[0].zaznaczone[0].y][dane[0].zaznaczone[0].x] = 'hit';
                            dane[0].plansza[dane[0].zaznaczone[1].y][dane[0].zaznaczone[1].x] = 'hit';
                        }
                        var tmp = 0
                        for (var i = 0; i < dane[0].plansza.length; i++) {
                            for (var j = 0; j < dane[0].plansza[i].length; j++) {
                                if (dane[0].plansza[i][j] == 'hit') {
                                    tmp++;
                                }
                            }
                        }
                        console.log(dane[0].plansza)
                        if (tmp < dane[0].plansza.length * dane[0].plansza[0].length) {
                            db.update('Game', {}, dane[0], 'Gra').then(() => {

                                io.to('game').emit('move_response', dane[0])
                            })
                        } else {
                            console.log('wyniki',dane[0].p1,dane[0].p2)
                            if (dane[0].p1 > dane[0].p2) {
                                io.to('game').emit('wyniki', { winner: 0 })
                            } else if (dane[0].p1 < dane[0].p2) {
                                io.to('game').emit('wyniki', { winner: 1 })
                            } else {
                                io.to('game').emit('wyniki', { winner: 2 })
                            }


                        }

                    }
                }
            }
        })
    })
})