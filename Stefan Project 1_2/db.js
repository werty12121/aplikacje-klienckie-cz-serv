var MongoClient = require('mongodb').MongoClient;
var ObjectID=require('mongodb').ObjectID
const url = "mongodb://localhost:27017/";
//const db_name = 'test61117';

var connect = async (dbn) => {
    
    let db = await MongoClient.connect(url + dbn);
    return db;
}
var disconnect = (datebase) => {
    try {
        datebase.close()
    } catch (e) {
        console.log(e)
    }
}
module.exports.get = async (collection, requirements, db) => {
    if(requirements._id!=undefined){
        requirements._id=new ObjectID(requirements._id)
    }
    let database = await connect(db);
    try {
        let collection_t = await database.collection(collection);
        let wynik;
       
        wynik = await collection_t.find(requirements).toArray();
        return wynik;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.insert = async (collection, data, db) => {
    let database = await connect(db);
    try {
        let collection_t = await database.collection(collection);
        let wynik = await collection_t.insertOne(data);
        return wynik.result;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.insertmany = async (collection, data_array, db) => {
    let database = await connect(db);
    try {
        let collection_t = await database.collection(collection);
        let wynik = await collection_t.insertMany(data_array)
        return wynik.result;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.update = async (collection, requirements, new_data, db) => {
    
    if(requirements._id!=undefined){
        requirements._id=new ObjectID(requirements._id)
    }
    let database = await connect(db);
    try {
        let collection_t = await database.collection(collection);
        let wynik = await collection_t.updateMany(requirements, { $set: new_data });
        return wynik.result;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.delete = async (collection, requirements, db) => {
    if(requirements._id!=undefined){
        requirements._id=new ObjectID(requirements._id)
    }
    let database = await connect(db);
    try {
        let collection_t = await database.collection(collection);
        let wynik = await collection_t.deleteMany(requirements);
        return wynik.result;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
module.exports.distinct = async (collection, key, requirements, db) => {
    if(requirements._id!=undefined){
        requirements._id=new ObjectID(requirements._id)
    }
    let database = await connect(db);
    try {
        let collection_t = await database.collection(collection);
        let wynik = await collection_t.distinct(key, requirements);
        return wynik;
    } catch (e) {
        return e
    } finally {
        disconnect(database);
    }
}
