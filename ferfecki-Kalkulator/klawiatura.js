import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Przycisk } from './przycisk'

export class Keyboard extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.container}>
                    <View style={styles.conteinerleft}>
                        <Przycisk styl={styles.button1} presskey='1' pressfunction={this.props.typeFun}></Przycisk>
                        <Przycisk styl={styles.button1} presskey='4' pressfunction={this.props.typeFun}></Przycisk>
                        <Przycisk styl={styles.button1} presskey='7' pressfunction={this.props.typeFun}></Przycisk>
                        <Przycisk styl={styles.button1} presskey='.' pressfunction={this.props.typeFun}></Przycisk>
                    </View>
                    <View style={styles.conteinerleft}>
                        <Przycisk styl={styles.button1} presskey='2' pressfunction={this.props.typeFun}></Przycisk>
                        <Przycisk styl={styles.button1} presskey='5' pressfunction={this.props.typeFun}></Przycisk>
                        <Przycisk styl={styles.button1} presskey='8' pressfunction={this.props.typeFun}></Przycisk>
                        <Przycisk styl={styles.button1} presskey='0' pressfunction={this.props.typeFun}></Przycisk>
                    </View>
                    <View style={styles.conteinerleft}>
                        <Przycisk styl={styles.button1} presskey='3' pressfunction={this.props.typeFun}></Przycisk>
                        <Przycisk styl={styles.button1} presskey='6' pressfunction={this.props.typeFun}></Przycisk>
                        <Przycisk styl={styles.button1} presskey='9' pressfunction={this.props.typeFun}></Przycisk>
                        <Przycisk styl={styles.button1} presskey='=' pressfunction={this.props.typeFun}></Przycisk>
                    </View>
                </View>
                <View style={styles.containerright}>
                    <Przycisk styl={styles.button2} presskey='C' pressfunction={this.props.typeFun}></Przycisk>
                    <Przycisk styl={styles.button2} presskey='/' pressfunction={this.props.typeFun}></Przycisk>
                    <Przycisk styl={styles.button2} presskey='*' pressfunction={this.props.typeFun}></Przycisk>
                    <Przycisk styl={styles.button2} presskey='-' pressfunction={this.props.typeFun}></Przycisk>
                    <Przycisk styl={styles.button2} presskey='+' pressfunction={this.props.typeFun}></Przycisk>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    button1: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#898989',
    },
    button2: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#676767',
    },
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#898989',
        justifyContent: 'center',
    },
    conteinerleft: {
        flex: 0.3,
        backgroundColor: '#898989',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    containerright: {
        flex: 0.3,
        flexDirection: 'column',
        backgroundColor: '#676767',
        justifyContent: 'center',
    },
})
