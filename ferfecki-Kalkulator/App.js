import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Keyboard } from './klawiatura'

export default class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = { dzialanie: 0, wynik: 0 }
    this.type = this.type.bind(this);
  }

  type(key) {
    switch(key){
      case "C":{
        var d = this.state.dzialanie.slice(0, -1)
        this.setState({
          dzialanie: d,
          wynik: 0
        })
        break
      }
      case "=":{
        try{
          var d=eval(this.state.dzialanie)
          this.setState({
            wynik: d
          })
        }catch(e){
          console.log(e)
        }
       
        break
      }
      default:{
        this.setState({
          dzialanie: this.state.dzialanie + key
        })
        break
      }
    }
  }
  render() {
    return (

      <View style={styles.container}>
        <View style={styles.wyswietlacz}>
          <Text style={styles.container1}>{this.state.dzialanie}</Text>
          <Text style={styles.container2}>{this.state.wynik}</Text>
        </View>
        <Keyboard typeFun={this.type}></Keyboard>
      </View>


    );
  }
}

const styles = StyleSheet.create({
  wyswietlacz: {
    flex: 0.4
  },
  container: {
    flex: 1,
    backgroundColor: '#ddd',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container2: {
    marginBottom: 20,
    flex: 0.3,
    fontSize: 35,
  },
  container1: {
    marginTop: 20,
    flex: 0.7,
    fontSize: 35,
  },
});
