class Hex {
    constructor(pos, rot, data) {
        this.position = pos;
        this.rotation = rot;
        this.doors = [data.in, data.out]
        this.position.y *= 1.73
        // console.log(this.position.x)
        // this.position.x-=Math.floor(this.position.x/2)
        // console.log(this.position.x)
        // if (this.position.x % 2 == 1) {

        //     this.position.y += 0.5
        // }//else{
        // //     this.position.x += 1
        // // }
        var pts = [];
        var x = 0,
            y = 0,
            z = 0;
        var radius = 50;
        if (this.position.x % 2 == 1) {
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius + 0 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius + 75, this.position.z * 1.73 * radius));
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius + radius * 0.866 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius * 0.5 + 75, this.position.z * 1.73 * radius));
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius + radius * 0.866 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius * 0.5 + 75, this.position.z * 1.73 * radius));
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius + 0 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius + 75, this.position.z));
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius - radius * 0.866 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius * 0.5 + 75, this.position.z * 1.73 * radius));
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius - radius * 0.866 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius * 0.5 + 75, this.position.z * 1.73 * radius));

        } else {
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius + 0 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius, this.position.z * 1.73 * radius));
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius + radius * 0.866 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius * 0.5, this.position.z * 1.73 * radius));
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius + radius * 0.866 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius * 0.5, this.position.z * 1.73 * radius));
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius + 0 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius, this.position.z));
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius - radius * 0.866 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius * 0.5, this.position.z * 1.73 * radius));
            pts.push(new THREE.Vector3(this.position.x * 1.73 * radius - radius * 0.866 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius * 0.5, this.position.z * 1.73 * radius));

        }

        this.hex = new THREE.Shape(pts);
        this.geometry = new THREE.ShapeGeometry(this.hex);
        this.material = new THREE.MeshBasicMaterial({
            color: 0x00ff00
        });

        var loader = new THREE.TextureLoader();
        var texture1 = loader.load("./mats/brick.jpg");
        this.material = new THREE.MeshPhongMaterial({ color: 0xffffff, map: texture1 });
        this.mesh = new THREE.Mesh(this.geometry, this.material);
        this.mesh.name = 'hex'
        this.mesh.rotation.set(-Math.PI * 2 / 360 * 90, 0, Math.PI * 2 / 360 * 90)

        scene.add(this.mesh);
        this.wall1 = new Walls({ x: this.position.x, y: this.position.y, z: this.position.z }, { x: this.rotation.x, y: this.rotation.y, z: this.rotation.z }, this.doors)
    }
    gwall(){
        return this.wall1.gwall()
    }
}
class Walls {
    constructor(pos, rot, doors) {
        this.position = pos;

        this.rotation = rot;
        this.doors = doors;
        var radius = 50
        this.light = new THREE.PointLight(0xffffff, 1, 100);

        if (this.position.x % 2 == 1) {
            var p1 = new THREE.Vector3(this.position.x * 1.73 * radius + 0 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius + 75, this.position.z * 1.73 * radius);
            var p2 = new THREE.Vector3(this.position.x * 1.73 * radius + radius * 0.866 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius * 0.5 + 75, this.position.z * 1.73 * radius);
            var p3 = new THREE.Vector3(this.position.x * 1.73 * radius + radius * 0.866 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius * 0.5 + 75, this.position.z * 1.73 * radius);
            var p4 = new THREE.Vector3(this.position.x * 1.73 * radius + 0 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius + 75, this.position.z);
            var p5 = new THREE.Vector3(this.position.x * 1.73 * radius - radius * 0.866 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius * 0.5 + 75, this.position.z * 1.73 * radius);
            var p6 = new THREE.Vector3(this.position.x * 1.73 * radius - radius * 0.866 - 45 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius * 0.5 + 75, this.position.z * 1.73 * radius);
            this.light.position.setX(this.position.x * 1.73 * radius - 45 - (90 * Math.floor(this.position.x / 2)))
            this.light.position.setY(this.position.y * 1.73 * radius)
            this.light.position.setZ(this.position.z * 1.73 * radius + 30)
        } else {
            var p1 = new THREE.Vector3(this.position.x * 1.73 * radius + 0 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius, this.position.z * 1.73 * radius);
            var p2 = new THREE.Vector3(this.position.x * 1.73 * radius + radius * 0.866 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius * 0.5, this.position.z * 1.73 * radius);
            var p3 = new THREE.Vector3(this.position.x * 1.73 * radius + radius * 0.866 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius * 0.5, this.position.z * 1.73 * radius);
            var p4 = new THREE.Vector3(this.position.x * 1.73 * radius + 0 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius, this.position.z);
            var p5 = new THREE.Vector3(this.position.x * 1.73 * radius - radius * 0.866 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius - radius * 0.5, this.position.z * 1.73 * radius);
            var p6 = new THREE.Vector3(this.position.x * 1.73 * radius - radius * 0.866 - (90 * Math.floor(this.position.x / 2)), this.position.y * 1.73 * radius + radius * 0.5, this.position.z * 1.73 * radius);
            this.light.position.setX(this.position.x * 1.73 * radius - (90 * Math.floor(this.position.x / 2)))
            this.light.position.setY(this.position.y * 1.73 * radius)
            this.light.position.setZ(this.position.z * 1.73 * radius + 30)
        }
        console.log(p1, p2, p3, p4, p5, p6)
        this.walls = new THREE.Object3D();
        this.geometry = new THREE.BoxGeometry(radius, radius / 2, 3);
        this.material = new THREE.MeshBasicMaterial({ color: 0x005577 });
        var loader = new THREE.TextureLoader();
        var texture1 = loader.load("./mats/brick.jpg");
        this.material = new THREE.MeshPhongMaterial({ color: 0xffffff, map: texture1 });
        if (this.doors[0] != 2 && this.doors[1] != 2) {
            this.obj1 = new THREE.Mesh(this.geometry, this.material);
            this.obj1.position.set((p2.x + p1.x) / 2, (p2.y + p1.y) / 2, (p2.z + p1.z) / 2 + radius / 4);
            this.obj1.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj1);
        }else{
            this.obj1_1 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_1.scale.setX(0.2)
            this.obj1_1.position.set(p2.x , p2.y , p2.z + radius / 4);
            this.obj1_1.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj1_1);
           
            this.obj1_2 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_2.scale.setX(0.2)
            this.obj1_2.position.set(p1.x,  p1.y, p1.z+ radius / 4);
            this.obj1_2.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj1_2);
        }

        if (this.doors[0] != 3 && this.doors[1] != 3) {
            this.obj2 = new THREE.Mesh(this.geometry, this.material);
            this.obj2.position.set((p2.x + p3.x) / 2, (p2.y + p3.y) / 2, (p2.z + p3.z) / 2 + radius / 4);
            this.obj2.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 90, this.rotation.z)
            this.walls.add(this.obj2);
        }else{
            this.obj1_1 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_1.scale.setX(0.2)
            this.obj1_1.position.set(p2.x , p2.y , p2.z + radius / 4);
            this.obj1_1.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 90, this.rotation.z)
            this.walls.add(this.obj1_1);
           
            this.obj1_2 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_2.scale.setX(0.2)
            this.obj1_2.position.set(p3.x,  p3.y, p3.z+ radius / 4);
            this.obj1_2.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 90, this.rotation.z)
            this.walls.add(this.obj1_2);
        }
        if (this.doors[0] != 4 && this.doors[1] != 4) {
            this.obj3 = new THREE.Mesh(this.geometry, this.material);
            this.obj3.position.set((p4.x + p3.x) / 2, (p4.y + p3.y) / 2, (p4.z + p3.z) / 2 + radius / 4);
            this.obj3.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y - Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj3);
        }
        else{
            this.obj1_1 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_1.scale.setX(0.2)
            this.obj1_1.position.set(p4.x , p4.y , p4.z + radius / 4);
            this.obj1_1.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y - Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj1_1);
           
            this.obj1_2 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_2.scale.setX(0.2)
            this.obj1_2.position.set(p3.x,  p3.y, p3.z+ radius / 4);
            this.obj1_2.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y - Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj1_2);
        }
        if (this.doors[0] != 5 && this.doors[1] != 5) {
            this.obj4 = new THREE.Mesh(this.geometry, this.material);
            this.obj4.position.set((p4.x + p5.x) / 2, (p4.y + p5.y) / 2, (p4.z + p5.z) / 2 + radius / 4);
            this.obj4.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj4);
        }else{
            this.obj1_1 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_1.scale.setX(0.2)
            this.obj1_1.position.set(p4.x , p4.y , p4.z + radius / 4);
            this.obj1_1.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj1_1);
           
            this.obj1_2 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_2.scale.setX(0.2)
            this.obj1_2.position.set(p5.x,  p5.y, p5.z+ radius / 4);
            this.obj1_2.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj1_2);
        }
        if (this.doors[0] != 0 && this.doors[1] != 0) {
            this.obj5 = new THREE.Mesh(this.geometry, this.material);
            this.obj5.position.set((p6.x + p5.x) / 2, (p6.y + p5.y) / 2, (p6.z + p5.z) / 2 + radius / 4);
            this.obj5.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 90, this.rotation.z)
            this.walls.add(this.obj5);
        }else{
            this.obj1_1 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_1.scale.setX(0.2)
            this.obj1_1.position.set(p6.x , p6.y , p6.z + radius / 4);
            this.obj1_1.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 90, this.rotation.z)
            this.walls.add(this.obj1_1);
           
            this.obj1_2 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_2.scale.setX(0.2)
            this.obj1_2.position.set(p5.x,  p5.y, p5.z+ radius / 4);
            this.obj1_2.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y + Math.PI * 2 / 360 * 90, this.rotation.z)
            this.walls.add(this.obj1_2);
        }
        if (this.doors[0] != 1 && this.doors[1] != 1) {
            this.obj6 = new THREE.Mesh(this.geometry, this.material);
            this.obj6.position.set((p1.x + p6.x) / 2, (p6.y + p1.y) / 2, (p1.z + p6.z) / 2 + radius / 4);
            this.obj6.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y - Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj6);
        }else{
            this.obj1_1 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_1.scale.setX(0.2)
            this.obj1_1.position.set(p6.x , p6.y , p6.z + radius / 4);
            this.obj1_1.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y - Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj1_1);
           
            this.obj1_2 = new THREE.Mesh(this.geometry, this.material);
            this.obj1_2.scale.setX(0.2)
            this.obj1_2.position.set(p1.x,  p1.y, p1.z+ radius / 4);
            this.obj1_2.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y - Math.PI * 2 / 360 * 210, this.rotation.z)
            this.walls.add(this.obj1_2);
        }

        console.log('asdasdasd', this.light.position)
        this.walls.add(this.light);
        this.walls.rotation.set(this.rotation.x - Math.PI * 2 / 360 * 90, this.rotation.y, this.rotation.z + Math.PI * 2 / 360 * 90)

        scene.add(this.walls)
    }
    gwall(){
        return this.walls.children
    }
}