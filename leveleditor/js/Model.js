class Model {
    constructor() {
        this.container = new THREE.Object3D()
        this.mixer
    }
    loadModel(urlmodel, urltexture, callback) {

        var loader = new THREE.JSONLoader();
        var that = this
        var meshModel
        var loader2 = new THREE.TextureLoader()
        loader2.load(urltexture, (texture) => {
            loader.load(urlmodel, function (geometry) {
                that.modelMaterial = new THREE.MeshBasicMaterial(
                    {
                        map: texture,
                        morphTargets: true // odpowiada za animację materiału modelu
                    });
                that.meshModel = new THREE.Mesh(geometry, that.modelMaterial)
                that.meshModel.name = "Player";
                that.meshModel.rotation.set(0, -Math.PI * 2 / 360 * 90, 0)  // ustaw obrót modelu
                that.meshModel.position.set(0, 20, 0)  // ustaw pozycje modelu
                that.meshModel.scale.set(1, 1, 1); // ustaw skalę modelu
                that.mixer = new THREE.AnimationMixer(that.meshModel);
                for (var i = 0; i < geometry.animations.length; i++) {
                   // console.log(geometry.animations[i].name);
                }
                var axes = new THREE.AxesHelper(100)
                //that.container.add(axes)
                that.container.add(that.meshModel)
                scene.add(that.container)
                // zwrócenie kontenera
                var bm=new THREE.MeshBasicMaterial({color:0xff0000,wireframe:true,side:THREE.DoubleSide})
                var boxg=new THREE.BoxGeometry(20,30,20)
                var box=new THREE.Mesh(boxg,bm)
                box.position.y+=15
                that.container.add(box)
                callback(that.container);
                
            });
        });
    }
    getmesh() {
        //console.log(this)
        return this.meshModel;
    }
    getContainer() {
        return this.container
    }
    // update mixera

    updateModel(delta) {

        if (this.mixer) this.mixer.update(delta)
    }

    //animowanie postaci

    setAnimation(anim) {

        try {
            //console.log(this.prev, anim)
            this.mixer.update(0)
            this.mixer.existingAction(this.prev).stop();
        } catch (e) {
            //console.log(e)
        }
        this.mixer.clipAction(anim).play();
        this.prev = anim;
    }

}