class Player {
    constructor() {
        this.container = new THREE.Object3D()
        this.player = new Model()
        this.player.loadModel('./mats/homer.js', './mats/homer.png', (e) => {
            this.container = e;
        })

        // this.container.add(this.player)
    }
    goToPoint(clickedVect, directionVect) {
        
        var angle = Math.atan2(
            this.player.container.position.clone().x - clickedVect.x,
            this.player.container.position.clone().z - clickedVect.z
        )
        this.player.getmesh().rotation.y = angle-Math.PI*2/360*90;
        this.player.setAnimation('run');

    }
    stand(){
        this.player.setAnimation('stand');
    }
    getPlayerCont() {
        return this.container
    }
    getPlayerMesh() {
        return this.player
    }
}