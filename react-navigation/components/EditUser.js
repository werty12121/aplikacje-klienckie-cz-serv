import React, { Component } from 'react';
import { Text, View ,StyleSheet,Image} from 'react-native';

export default class EditUser extends Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (
            <View style={styles.view}>
                <Image
                    source={require('../assets/img/default.png')}
                    style={styles.img}
                />
                <Text style={styles.text}>{this.props.navigation.state.params.username}</Text>
                <Text style={styles.text}>{this.props.navigation.state.params.passowrd}</Text>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    text:{
        textAlign:"center",
        alignItems:"center",
    },
    view:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    img:{
        width:300,
        height:300
    }
})