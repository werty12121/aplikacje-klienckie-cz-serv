import { createStackNavigator } from "react-navigation";
import Users from "./components/Users"
import EditUser from "./components/EditUser"
import Home from "./components/Home"

const App = createStackNavigator({
  Home: {
     screen: Home,
     navigationOptions:{
      title: "Home",
      headerStyle: {
        backgroundColor: "#000000",
      },
      headerTitleStyle: {
        color: "#ffffff"
      }
     }
    },
  'Users': { screen:   Users },
  'EditUser': { screen: EditUser },
});

export default App;