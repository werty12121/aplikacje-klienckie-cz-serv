const express = require('express')
const mongo= require('./db')
const app = express()
const port = 3000
const bodyParser = require('body-parser');

const url="mongodb://localhost:27017/"
const db='stef'

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
app.post('/register', function (req, res) {
    console.log('register')
    mongo.get(url,"users",{username:req.body.username},0,0,0,db).then((data)=>{
        console.log(data)
        if(data.length==0){
            return mongo.insert(url,"users",{username:req.body.username,password:req.body.password},db)
            //res.send('OK')
        }else{
            return "User exist"
            //res.send("Error: user already exist!!")
        }
    }).then((data)=>{
        if(data!= "User exist"){
            console.log(data)
            res.send({status:'OK'})
        }else{
            res.send({status:"Error: user already exist!!"})
        }
    })
})
app.post('/get',(req,res)=>{
    console.log('get')
    mongo.get(url,"users",req.body.req,0,0,0,db).then((data)=>{
        if(data.length!=0){
            res.send({data:data})
        }else{
            res.send({data:[]})
        }
    })
})
app.post('/delete',(req,res)=>{
    console.log('delete')
    mongo.delete(url,"users",req.body.req,db).then((data)=>{
        res.send(data)
    })
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))