Instalacja:
    -wpisanie komendy "npm i"
    -wypełnienie pliku config.json
    -zmiana adresu w pliku /public/javascripts/connect.js w pierwszej linijce na adres servera
    -zmiana adresu w pliku /public/javascripts/login.js w pierwszej linijce na adres servera
    -zmiana adresu w pliku /public/javascripts/Net.js w pierwszej linijce na adres servera
    -zmiana adresu w pliku /public/javascripts/register.js w pierwszej linijce na adres servera
    -zmiana adresu w pliku /connect.js w 111 linijce na adres servera mongodb
    -uwaga by przesyłanie emaili działo nasza skrzynka pocztowa musi być do tego przystosowana (w gmailu wystarczy włączyć dostęp mniej bezpiecznym aplikacjom),
    w przeciwnym wypadku veryfikowanie użytkownika musi się odbywać w sposób ręczny (poprzez zmianę statusu konta na "verified")
    -na serverze mongodb trzeba utworzyć bazę danych o nazwie "pkr" oraz utworzyć w niej puste kolecke o nazwach : "Games","Konta","Sessions"

Na koniec wpisać komendę "npm start".
Server wystartuje na localhostcie na porcie podanym wcześniej w pliku config.json .

Autorzy : 
    Bartosz Przechera
    Maciej Kosćielniak 
wszelkie prawa zastrzeżone

Licencja :
    Beerware

W razie błędów pisać na adres :
    BartoszPrzechera@gmail.com