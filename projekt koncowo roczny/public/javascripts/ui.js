$(document).mousemove(() => {
    mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
    mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
    raycaster.setFromCamera(mouseVector, camera);
    var intersects = raycaster.intersectObjects(planearr);
    if (intersects.length > 0) {
        if (start_bool) {
            if (board[intersects[0].object.plansza.y][intersects[0].object.plansza.x] == 0) {
                end_coords.x = intersects[0].object.plansza.x
                end_coords.y = intersects[0].object.plansza.y
                checkRoad()
            }
        }
    }
})
$(document).keypress((event) => {
    var keyCode = event.which;
    if (keyCode == 65 || keyCode == 97) {
        kat -= Math.PI / 180
    } else if (keyCode == 68 || keyCode == 100) {
        kat += Math.PI / 180
    }
    else if (keyCode == 119 || keyCode == 87) {
        if (odl > 200)
            odl -= 3

    } else if (keyCode == 83 || keyCode == 115) {
        if (odl < 600)
            odl += 3
    }
    console.log(keyCode)
})
$(document).mousedown(function (event) {
    for (var i = 0; i < drogaarr.length; i++) {
        scene.remove(drogaarr[i])
    }
    drogaarr = []
    mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
    mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
    raycaster.setFromCamera(mouseVector, camera);
    var intersects = raycaster.intersectObjects(planearr);

    if (intersects.length > 0) {
        try {
            prevobj.material = prevmat
        } catch (e) {
        }
        prevobj = intersects[0].object
        prevmat = intersects[0].object.material
        console.log(scene.children.length)
        console.log(intersects[0].object)
        if (board[intersects[0].object.plansza.y][intersects[0].object.plansza.x] != 0) {

            if (!start_bool) {
                console.log('a', start_bool, intersects[0].object.material, selectedMaterial)
                start_bool = true;
                start_coords.x = intersects[0].object.plansza.x
                start_coords.y = intersects[0].object.plansza.y
                intersects[0].object.material = selectedMaterial;

            } else {
                if (start_coords.x != intersects[0].object.plansza.x || start_coords.y != intersects[0].object.plansza.y) {
                    console.log('b', start_bool)
                    start_coords.x = intersects[0].object.plansza.x
                    start_coords.y = intersects[0].object.plansza.y
                } else {
                    console.log('c', start_bool)
                    start_coords.x = undefined
                    start_coords.y = undefined
                    start_bool = false;
                }
            }
        } else {
            if (start_bool) {
                console.log('d', start_bool)
                end_coords.x = intersects[0].object.plansza.x
                end_coords.y = intersects[0].object.plansza.y
                console.log('move')
                start_bool = false;
                checkRoad()

            }
        }
    }
})