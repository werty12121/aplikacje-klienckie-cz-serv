function checkRoad() {
    for (var i = 0; i < boardHeight; i++) {
        Listy_sasiedztwa.push([])
        for (var j = 0; j < boardWidth; j++) {
            Listy_sasiedztwa[i].push([]);
            if (board[i][j] != undefined) {
                if (i - 1 >= 0 && board[i - 1][j] == 0) {
                    Listy_sasiedztwa[i][j].push({ y: i - 1, x: j })
                }
                if (i + 1 < boardHeight && board[i + 1][j] == 0) {
                    Listy_sasiedztwa[i][j].push({ y: i + 1, x: j })
                }
                if (j - 1 >= 0 && board[i][j - 1] == 0) {
                    Listy_sasiedztwa[i][j].push({ y: i, x: j - 1 })
                }
                if (j + 1 < boardWidth && board[i][j + 1] == 0) {
                    Listy_sasiedztwa[i][j].push({ y: i, x: j + 1 })
                }
            }
        }
    }

    var x = start_coords.x, y = start_coords.y;
    // if (!start_bool)
    // 	plansza[y][x] = CONFIG.ROCK;

    if (najkrotszadroga(x, y) != 'OK') {

        if (start_bool) {
            // for (var j = 0; j < pola.length; j++) {
            // 	pola[j].setAttribute("style", "")
            // }
            for (var i = 0; i < drogaarr.length; i++) {
                scene.remove(drogaarr[i])
            }
            drogaarr = []
        } else {
            // for (var j = 0; j < pola.length; j++) {
            // 	if (pola[j].getAttribute('name') == start_coords) {
            // 		pola[j].childNodes[1].className = 'kulka';
            // 	}
            // }
            try {
                prevobj.material = prevmat
            } catch (e) {

            }
            start_coords = {};
            end_coords = {}
        }
        Listy_sasiedztwa = []
        p = [];
        odw = [];
        for (var i = 0; i < boardHeight; i++) {
            p.push([])
            odw.push([])
            for (var j = 0; j < boardWidth; j++) {
                p[i].push(null)
                odw[i].push(null)
            }
        }
    }
}

function najkrotszadroga(x, y) {

    var p_t = p;
    var q = [];
    var odw_t = odw;
    p_t[y][x] = -1;
    q.push({ x: x, y: y })
    odw_t[y][x] = true;
    while (q.length != 0) {
        var v = q[0];
        var v_list = Listy_sasiedztwa[v.y][v.x]
        q.shift();

        if (v.x == end_coords.x && v.y == end_coords.y) {
            wyswietl(v);
            console.log('OK')
            return 'OK';
        }
        for (var i = 0; i < v_list.length; i++) {

            if (odw_t[v_list[i].y][v_list[i].x] == true) {
            } else {
                p_t[v_list[i].y][v_list[i].x] = v;
                q.push(v_list[i])
                odw_t[v_list[i].y][v_list[i].x] = true;
            }
        }
    }
    console.log('brak')
    return "BRAK"
    function wyswietl(v) {
        var temp_string = [];
        while (v != -1) {
            temp_string.push(v)
            v = p_t[v.y][v.x]
            if (v <= -1) {
                console.log(temp_string)
                if (start_bool) {
                    nanies(temp_string);

                } else {
                    nanies2(temp_string);
                }
                Listy_sasiedztwa = []
                p = [];
                odw = [];
                for (var i = 0; i < boardHeight; i++) {
                    p.push([])
                    odw.push([])
                    for (var j = 0; j < boardWidth; j++) {
                        p[i].push(null)
                        odw[i].push(null)
                    }
                }
            }
        }
    }
}
function nanies(tab) {
    for (var i = 0; i < drogaarr.length; i++) {
        scene.remove(drogaarr[i])
    }
    drogaarr = []
    for (var i = 0; i < tab.length; i++) {
        var geometry = new THREE.PlaneGeometry(30, 30);
        var material = new THREE.MeshPhongMaterial({
            color: "rgba(0, 140, 0,0.4)",
            wireframe: false,
            side: THREE.DoubleSide,
            specular: 0x000000,
            shininess: 0.01,
        })

        var plane = new THREE.Mesh(geometry, material);
        plane.position.set(tab[i].x * 30 - (boardWidth / 2 * 30), 5, tab[i].y * 30 - (boardHeight / 2 * 30))
        plane.rotation.x = Math.PI / 2
        plane.plansza = {}
        plane.plansza.x = tab[i].x
        plane.plansza.y = tab[i].y
        scene.add(plane);
        drogaarr.push(plane)

    }
}
function nanies2(tab) {

    // board[end_coords.y][end_coords.x] = board[start_coords.y][start_coords.x]
    // board[start_coords.y][start_coords.x] = 0;
    // loadTable(board)
    net.move(start_coords,end_coords, roomName, session )
    start_bool = false;
}