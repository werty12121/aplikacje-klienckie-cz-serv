
var net = new Net()
net.jointoroom(roomName)

window.onbeforeunload = endGame;
if (session.length == 0) {
	window.location.href = "./login";
}
if (roomName.length == 0) {
	window.location.href = "./connect";
}

leaveButton.onclick = endGame;
function endGame() {
	net.endgame(roomName)
	deleteCookie("room_name");
	deleteCookie("player");
	window.location.href = "./connect";
}

renderer.setClearColor(0xffffff);
renderer.setSize($(document).width(), $(document).height());
$("#root").append(renderer.domElement);
camera.position.set(odl, odl, 0)
camera.lookAt(scene.position);
var axes = new THREE.AxesHelper(1000)
//scene.add(axes)
var clock = new THREE.Clock();
var skyboxgeo = new THREE.BoxGeometry(10000, 10000, 10000)
var skyboxmatarr = []
skyboxmatarr.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('png/as_lf.png') }));
skyboxmatarr.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('png/as_rt.png') }));
skyboxmatarr.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('png/as_up.png') }));
skyboxmatarr.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('png/as_dn.png') }));
skyboxmatarr.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('png/as_ft.png') }));
skyboxmatarr.push(new THREE.MeshBasicMaterial({ side: THREE.DoubleSide, map: new THREE.TextureLoader().load('png/as_bk.png') }));
var skymesh = new THREE.Mesh(skyboxgeo, skyboxmatarr);
scene.add(skymesh)
var light = new THREE.SpotLight(0xffffff, 0.1, 50, 3.14);
light.position.set(0, 500, 0);
scene.add(light);
function render() {
	var delta = clock.getDelta();
	requestAnimationFrame(render);
	skymesh.position.set(camera.position.x, camera.position.y - 300, camera.position.z)
	camera.position.z = odl * Math.cos(kat);
	camera.position.x = odl * Math.sin(kat);
	camera.lookAt(scene.position)
	renderer.render(scene, camera);

}
render();
var loader = new THREE.ObjectLoader();
// load a resource
loader.load('/obj/model.json', function (object) {
	var modelMaterial = new THREE.MeshPhongMaterial(
		{
			map: THREE.ImageUtils.loadTexture("/png/WoodSeemles.jpg"),
			morphTargets: true,
			specular: 0x000000,
			shininess: 0.01,
		});
	object.material = modelMaterial
	object.name = "table";
	object.rotation.set(0, 0, 0)
	object.position.set(0, -125, 0)  // ustaw pozycje modelu
	object.scale.set(2.5, 2, 2); // ustaw skalę modelu
	scene.add(object);

}
);
var selectedMaterial = new THREE.MeshPhongMaterial({
	color: 0xff0000, side: THREE.DoubleSide,
	specular: 0x000000,
	shininess: 0.01,
})
var prevobj;
var prevmat;



function loadTable(board) {
	planeboard = [];
	for (var i = 0; i < drogaarr.length; i++) {
		scene.remove(drogaarr[i])
	}
	for (var i = 0; i < planearr.length; i++) {
		scene.remove(planearr[i])
	}
	for (var i = 0; i < kulkiarr.length; i++) {
		scene.remove(kulkiarr[i])
	}
	planearr = []
	kulkiarr = []
	drogaarr = []
	boardWidth = board[0].length;
	boardHeight = board.length;
	var materialA = new THREE.MeshPhongMaterial({
		color: 0xeeeeee,
		side: THREE.DoubleSide,
		ambient: 0x000000,
		specular: 0x000000,
		shininess: 0.01,
	});
	var materialB = new THREE.MeshPhongMaterial({
		color: 0x000000,
		side: THREE.DoubleSide,
		ambient: 0x000000,
		specular: 0x000000,
		shininess: 0.01,
	});
	for (var i = 0; i < boardHeight; i++) {
		planeboard.push([])
		odw.push([])
		p.push([])
		for (var j = 0; j < boardWidth; j++) {
			odw[i].push(null)
			p[i].push(null)
			var geometry = new THREE.BoxGeometry(30, 30, 30);
			var material
			//console.log(((i * boardWidth) + j + i))
			if (((i * boardWidth) + j + i) % 2 == 0) {
				material = materialA
			} else {
				material = materialB
			}
			var plane = new THREE.Mesh(geometry, material);
			plane.position.set(j * 30 - (boardWidth / 2 * 30), -15, i * 30 - (boardHeight / 2 * 30))
			plane.rotation.x = Math.PI / 2
			plane.plansza = {}
			plane.plansza.x = j
			plane.plansza.y = i
			scene.add(plane);
			planeboard.push(plane);
			planearr.push(plane);
			if (board[i][j] != 0) {
				var geometry = new THREE.SphereGeometry(10, 64, 64);
				var material = new THREE.MeshPhongMaterial({
					color: colors[board[i][j]],
					specular: 0x000000,
					shininess: 0.01,
					side: THREE.DoubleSide
				});
				var sphere = new THREE.Mesh(geometry, material);
				sphere.position.set(j * 30 - (boardWidth / 2 * 30), 10, i * 30 - (boardHeight / 2 * 30))
				sphere.plansza = {}
				sphere.plansza.x = j
				sphere.plansza.y = i
				scene.add(sphere);
				kulkiarr.push(sphere)
			}
		}
	}

}



