import React from 'react';
import { StyleSheet, Text, View,TouchableOpacity,Image } from 'react-native';
import { red } from 'ansi-colors';
import {Keyboard} from './keyboard'

export default class App extends React.Component {
  
  constructor(props) {
    super(props)
    this.state = { text: 0,wynik:0 }
    this.type = this.type.bind(this);
  }
  
  type(key){
    if(key=='C'){
      console.log()
      this.setState({
        text:this.state.text.slice(0, -1),
        wynik:0
      })
    }else if(key=="="){
      try{
        this.setState({
          wynik:eval(this.state.text)
        })
      }catch(e){
        console.log(e)
      }
     
    }else{
      this.setState({
        text:this.state.text+key
      })
    }
   
  }
  render() {
    return (
      
        <View style={styles.container}>
          <View style={styles.wys}>
            <Text style={styles.ct1}>{this.state.text}</Text>
            <Text style={styles.ct2}>{this.state.wynik}</Text>
          </View>
          <Keyboard typeFun={this.type}></Keyboard>
        </View>
        
     
    );
  }
}

const styles = StyleSheet.create({
  wys:{
    flex:0.3
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  }, 
  ct2:{
    flex:0.3,
    fontSize: 35,
},
ct1:{
  flex:0.6,
  fontSize: 35,
},
});
