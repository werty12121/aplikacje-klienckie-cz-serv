import React from 'react';
import { StyleSheet, Text, View,TouchableOpacity,Image } from 'react-native';

export class Keyboard extends React.Component{
    constructor(props){
        super(props)
    }
    render() {
        return (
            <View style={styles.container}>
                <KeyboardLeft typeFun={this.props.typeFun}></KeyboardLeft>
                <KeyboardRight typeFun={this.props.typeFun} ></KeyboardRight>
            </View>
        )
    }
}
class KeyboardLeft extends React.Component{
    constructor(props){
        super(props)
        this.typing = this.typing.bind(this);
    }
    typing(key){
        this.props.typeFun(key)
    }
    render() {
        return (
            <View style={styles.container}>
               <View style={styles.c1}>
                    <Button style={styles.button1} v='1' press={this.typing}></Button>
                    <Button style={styles.button1} v='4' press={this.typing}></Button>
                    <Button style={styles.button1} v='7' press={this.typing}></Button>
                    <Button style={styles.button1} v='.' press={this.typing}></Button>
                </View>
                <View style={styles.c1}>
                    <Button style={styles.button1} v='2' press={this.typing}></Button>
                    <Button style={styles.button1} v='5' press={this.typing}></Button>
                    <Button style={styles.button1} v='8' press={this.typing}></Button>
                    <Button style={styles.button1} v='0' press={this.typing}></Button>
                </View>
                <View style={styles.c1}>
                    <Button style={styles.button1} v='3' press={this.typing}></Button>
                    <Button style={styles.button1} v='6' press={this.typing}></Button>
                    <Button style={styles.button1} v='9' press={this.typing}></Button>
                    <Button style={styles.button1} v='=' press={this.typing}></Button>
                </View>
            </View>

        )
    }
}
class KeyboardRight extends React.Component{
    constructor(props){
        super(props)
        this.typing = this.typing.bind(this);
    }
    typing(key){
       this.props.typeFun(key)
    }
    render() {
        return(
            <View style={styles.c2}>
                <Button style={styles.button2} v='C' press={this.typing}></Button>
                <Button style={styles.button2} v='/' press={this.typing}></Button>
                <Button style={styles.button2} v='*' press={this.typing}></Button>
                <Button style={styles.button2} v='-' press={this.typing}></Button>
                <Button style={styles.button2} v='+' press={this.typing}></Button>
            </View>
                
                
           
        )
    }
}
class Button extends React.Component{
    constructor(props){
        super(props)
        this.press = this.press.bind(this);
    }
    press(){
        this.props.press(this.props.v)
    }
    render() {
        return (
            <TouchableOpacity style={this.props.style} onPress={this.press}>
                <Text style={styles.ct}>{this.props.v}</Text>
            </TouchableOpacity>
        )
    }
}
const styles=StyleSheet.create({
    button1: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#999',
      },
      button2: {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#666',
      },
    container:{
        flex: 1,
        flexDirection: 'row',
        backgroundColor: '#999999',
        justifyContent: 'center',
    },
    c1:{
        flex: 0.3,
        backgroundColor: '#999',
        flexDirection: 'column',
        justifyContent: 'center',
    },
    c2:{
        flex: 0.3,
        flexDirection: 'column',
        backgroundColor: '#666666',
        justifyContent: 'center',
    },
    ct:{
        fontSize: 40,
    },
})
